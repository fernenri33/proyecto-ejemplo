import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class App {
    public static void main(String[] args) {
        Persona p1 = new Persona("Luka", "Modric", 10);
        Persona p2 = new Persona("Karim", "Benzema", 9);
        Persona p3 = new Persona("Marco", "Asensio", 20);
    
        List<Persona> personas = new ArrayList<Persona>();
        personas.add(p1);
        personas.add(p2);
        personas.add(p3);

        //Collections.sort(personas,Collections.reverseOrder());
        Collections.sort(personas);

        for(Persona elemento:personas){
            System.out.println(elemento);
        }
    
    }   
}
